var graph = require('@microsoft/microsoft-graph-client');

var xdata =
{
  '6dad4270-fa7d-4bd4-97ba-dfa4151f36f9':
  {
    id: '6dad4270-fa7d-4bd4-97ba-dfa4151f36f9',
    displayName: 'Ádám Gaugecz',
    jobTitle: 'Product Manager',
    mail: 'Adam.Gaugecz@memoQ.com',
    mobilePhone: null,
    officeLocation: null,
    department: 'Production',
    city: null,
    country: null,
    mgrId: '17e6bcc1-121b-4693-a4a8-c82cbe6448d1'
  },
  '0190a4a9-c94b-4431-9366-3b76a8667a6d':
  {
    id: '0190a4a9-c94b-4431-9366-3b76a8667a6d',
    displayName: 'Ádám Kovács',
    jobTitle: 'UI Designer',
    mail: 'Adam.Kovacs@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '19fe9d6f-3bb6-483d-969d-0835618b4d82':
  {
    id: '19fe9d6f-3bb6-483d-969d-0835618b4d82',
    displayName: 'Ádám Kulcsár',
    jobTitle: 'Software Developer',
    mail: 'Adam.Kulcsar@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  'b7f161a4-9783-4593-b635-0f1cd63546f8':
  {
    id: 'b7f161a4-9783-4593-b635-0f1cd63546f8',
    displayName: 'Ágnes Varga',
    jobTitle: 'Software Developer',
    mail: 'Agnes.Varga@memoQ.com',
    mobilePhone: '+36 20 212 60 38',
    officeLocation: 'Győr, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  'ab86d32e-eb82-41e5-b875-1609f6224018':
  {
    id: 'ab86d32e-eb82-41e5-b875-1609f6224018',
    displayName: 'Ákos Demeter',
    jobTitle: 'Solution Engineer',
    mail: 'Akos.Demeter@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '9b59c01a-8423-4d3f-abc1-2d115f7fc251'
  },
  '4506a445-927f-4d2d-a182-bb177a1a1515':
  {
    id: '4506a445-927f-4d2d-a182-bb177a1a1515',
    displayName: 'Alexandra Princzkel',
    jobTitle: 'Inbound Marketing Manager',
    mail: 'Alexandra.Princzkel@memoQ.com',
    mobilePhone: '+36 70 944 2034',
    officeLocation: null,
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  'd439a64c-0adf-4380-80f5-b57db8ab1cbb':
  {
    id: 'd439a64c-0adf-4380-80f5-b57db8ab1cbb',
    displayName: 'Andor Nagy',
    jobTitle: 'Support Engineer',
    mail: 'Andor.Nagy@memoQ.com',
    mobilePhone: '+36 70 662 10 61',
    officeLocation: 'Gyula, Hungary',
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  '98225f9b-cca7-4d83-9fe1-2688b2f99d1c':
  {
    id: '98225f9b-cca7-4d83-9fe1-2688b2f99d1c',
    displayName: 'Andor Plank',
    jobTitle: 'Junior Support Engineer',
    mail: 'Andor.Plank@memoQ.com',
    mobilePhone: '+36 20 386 7200',
    officeLocation: 'Gyula, Hungary',
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  '8614c1bf-4382-4832-94d2-0df83fc80015':
  {
    id: '8614c1bf-4382-4832-94d2-0df83fc80015',
    displayName: 'András Csábi',
    jobTitle: 'Infrastructure Specialist',
    mail: 'Andras.Csabi@memoQ.com',
    mobilePhone: '+36 30 249 10 73',
    officeLocation: 'Budapest, Hungary',
    department: 'Services & IT',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: 'b30295c2-10cd-4442-8dcf-02c661deb531'
  },
  '8c6f3ea3-df73-498f-9b0d-d68449b5b6f0':
  {
    id: '8c6f3ea3-df73-498f-9b0d-d68449b5b6f0',
    displayName: 'András Földes',
    jobTitle: 'Senior Software Developer',
    mail: 'Andras.Foldes@memoQ.com',
    mobilePhone: '+36 20 977 8136',
    officeLocation: 'Budapest, Hiungary',
    department: 'Production',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '47c2c9a2-de58-4f7a-a9e4-44a51f078bf2':
  {
    id: '47c2c9a2-de58-4f7a-a9e4-44a51f078bf2',
    displayName: 'Anett Guth',
    jobTitle: 'Market Researcher',
    mail: 'Anett.Guth@memoQ.com',
    mobilePhone: '+36205613595',
    officeLocation: null,
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  'e3c97442-73a9-4e7e-90e3-d789331c6e83':
  {
    id: 'e3c97442-73a9-4e7e-90e3-d789331c6e83',
    displayName: 'Angéla Botta',
    jobTitle: 'Financial Assistant',
    mail: 'Angela.Botta@memoQ.com',
    mobilePhone: '+361 808 8313 /114',
    officeLocation: 'Gyula, Hungary',
    department: 'Finance',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '8900a9ef-08da-4c75-9edd-a47430ad397b'
  },
  'f3f0ba9b-f561-44a9-9cdb-01a1cb6b97a8':
  {
    id: 'f3f0ba9b-f561-44a9-9cdb-01a1cb6b97a8',
    displayName: 'Barnabás Nagy',
    jobTitle: 'Software Developer',
    mail: 'Barnabas.Nagy@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  'c2ce7ecf-dcfa-432d-b8fa-6d7ad9d54834':
  {
    id: 'c2ce7ecf-dcfa-432d-b8fa-6d7ad9d54834',
    displayName: 'Beáta Szekeres',
    jobTitle: 'Market Researcher',
    mail: 'Beata.Szekeres@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '688b558d-99a8-46eb-babe-4a9c43d36c20':
  {
    id: '688b558d-99a8-46eb-babe-4a9c43d36c20',
    displayName: 'Beatrice Compagnon',
    jobTitle: 'Account Manager',
    mail: 'Beatrice.Compagnon@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Contractor',
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  'b52dd1de-1b87-4fa0-a3e2-6f71a2dd6d0c':
  {
    id: 'b52dd1de-1b87-4fa0-a3e2-6f71a2dd6d0c',
    displayName: 'Bernadett Sütő',
    jobTitle: 'Office Coordinator',
    mail: 'Bernadett.Suto@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'HR',
    city: null,
    country: null,
    mgrId: '411ddde4-c070-469a-8c1d-f615b0b9a85c'
  },
  '8a1f1ce0-53d4-4640-83d2-e503f65ad00b':
  {
    id: '8a1f1ce0-53d4-4640-83d2-e503f65ad00b',
    displayName: 'Bernardo Santos',
    jobTitle: 'Director Of Translation Company Sales, EMEA',
    mail: 'Bernardo.Santos@memoQ.com',
    mobilePhone: '+35 191 876 57 25',
    officeLocation: 'Porto, Portugal',
    department: 'Smarketing',
    city: 'Porto',
    country: 'Portugal',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '33b3f0ee-52cc-409f-a294-ae76f2397b96':
  {
    id: '33b3f0ee-52cc-409f-a294-ae76f2397b96',
    displayName: 'Brúnó Bitter',
    jobTitle: 'Head of Smarketing',
    mail: 'Bruno.Bitter@memoQ.com',
    mobilePhone: '+36 30 370 0148',
    officeLocation: null,
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '411ddde4-c070-469a-8c1d-f615b0b9a85c'
  },
  'f4857bf4-1762-4470-a1f2-d6698b0d3747':
  {
    id: 'f4857bf4-1762-4470-a1f2-d6698b0d3747',
    displayName: 'Csaba Cserép',
    jobTitle: 'Innovation Lead',
    mail: 'Csaba.Cserep@memoQ.com',
    mobilePhone: '+36 30 475 7356',
    officeLocation: 'Budapest, Hungary',
    department: 'Innovation',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '768c5fce-d30e-4998-9dd4-0b1fc08c5f78'
  },
  '57ad1e51-f335-44e5-89b1-b243865965fd':
  {
    id: '57ad1e51-f335-44e5-89b1-b243865965fd',
    displayName: 'Damien Saby',
    jobTitle: 'Digital Marketing Executive',
    mail: 'Damien.Saby@memoQ.com',
    mobilePhone: '+33 (0)6 12 88 44 99',
    officeLocation: null,
    department: 'Smarketing',
    city: null,
    country: 'France',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  'ee19eaca-29fb-48d9-8b53-eca82984f967':
  {
    id: 'ee19eaca-29fb-48d9-8b53-eca82984f967',
    displayName: 'Dániel Bodonyi',
    jobTitle: 'Product Manager',
    mail: 'Daniel.Bodonyi@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '17e6bcc1-121b-4693-a4a8-c82cbe6448d1'
  },
  'b953dd45-d327-40de-a35f-45b067dfcadb':
  {
    id: 'b953dd45-d327-40de-a35f-45b067dfcadb',
    displayName: 'Dániel Nagy',
    jobTitle: 'Academic and Individual Translator Sales',
    mail: 'Daniel.Nagy@memoQ.com',
    mobilePhone: '+36 20 389 39 30',
    officeLocation: null,
    department: 'Smarketing',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '0486d872-d035-42fe-95ea-77bfd7517769':
  {
    id: '0486d872-d035-42fe-95ea-77bfd7517769',
    displayName: 'Dávid Farkas',
    jobTitle: 'System Administrator',
    mail: 'David.Farkas@memoQ.com',
    mobilePhone: '+36 30 619 68 79',
    officeLocation: null,
    department: 'Services & IT',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: 'b30295c2-10cd-4442-8dcf-02c661deb531'
  },
  '363a854b-61d0-411d-942d-a2cfa9fbde3b':
  {
    id: '363a854b-61d0-411d-942d-a2cfa9fbde3b',
    displayName: 'Dominika Olszewska',
    jobTitle: 'Sales Executive',
    mail: 'Dominika.Olszewska@memoQ.com',
    mobilePhone: '+48518811891',
    officeLocation: 'Poland',
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  'a33d98c0-e083-48c3-9cab-fbb02ba0dbf6':
  {
    id: 'a33d98c0-e083-48c3-9cab-fbb02ba0dbf6',
    displayName: 'Engracia Batista',
    jobTitle: 'Business Development Representative',
    mail: 'Engracia.Batista@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Toronto, Canada',
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  'a11f3828-8bcd-43fb-9752-eda171f09df4':
  {
    id: 'a11f3828-8bcd-43fb-9752-eda171f09df4',
    displayName: 'Erzsébet Kolcsár',
    jobTitle: 'HR Coordinator',
    mail: 'Erzsebet.Kolcsar@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'HR',
    city: null,
    country: null,
    mgrId: '411ddde4-c070-469a-8c1d-f615b0b9a85c'
  },
  'f3f577b9-8fc3-476d-a3e4-9a9845e249b6':
  {
    id: 'f3f577b9-8fc3-476d-a3e4-9a9845e249b6',
    displayName: 'Ferenc Szendi',
    jobTitle: 'Senior Software Developer',
    mail: 'Ferenc.Szendi@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '17e6bcc1-121b-4693-a4a8-c82cbe6448d1':
  {
    id: '17e6bcc1-121b-4693-a4a8-c82cbe6448d1',
    displayName: 'Florian Sachse',
    jobTitle: 'Head Of Production',
    mail: 'Florian.Sachse@memoQ.com',
    mobilePhone: '+49 172 255 97 08',
    officeLocation: 'Bonn, Germany',
    department: 'Production',
    city: 'Bonn',
    country: 'Germany',
    mgrId: '411ddde4-c070-469a-8c1d-f615b0b9a85c'
  },
  '9b59c01a-8423-4d3f-abc1-2d115f7fc251':
  {
    id: '9b59c01a-8423-4d3f-abc1-2d115f7fc251',
    displayName: 'Gábor Fauszt',
    jobTitle: 'Client Service Director',
    mail: 'Gabor.Fauszt@memoQ.com',
    mobilePhone: '+36202837903',
    officeLocation: 'Budapest, Hungary',
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '6bf33287-7226-45f6-9638-36f7173b57cd':
  {
    id: '6bf33287-7226-45f6-9638-36f7173b57cd',
    displayName: 'Gábor Kovács',
    jobTitle: 'Solution Engineer',
    mail: 'Gabor.Kovacs@memoQ.com',
    mobilePhone: '+36703152384',
    officeLocation: null,
    department: 'Smarketing',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '9b59c01a-8423-4d3f-abc1-2d115f7fc251'
  },
  '1226ebbf-69fe-48de-9326-d3ceaf5849d4':
  {
    id: '1226ebbf-69fe-48de-9326-d3ceaf5849d4',
    displayName: 'Gábor Nagy',
    jobTitle: 'Support Engineer',
    mail: 'Gabor.Nagy@memoQ.com',
    mobilePhone: '+36 30 841 29 60',
    officeLocation: 'Gyula, Hungary',
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  '768c5fce-d30e-4998-9dd4-0b1fc08c5f78':
  {
    id: '768c5fce-d30e-4998-9dd4-0b1fc08c5f78',
    displayName: 'Gábor Ugray',
    jobTitle: 'Head of Innovation',
    mail: 'Gabor.Ugray@memoQ.com',
    mobilePhone: '+49 157 5438 6918',
    officeLocation: 'Berlin, Germany',
    department: 'Innovation',
    city: 'Berlin',
    country: 'Germany',
    mgrId: '411ddde4-c070-469a-8c1d-f615b0b9a85c'
  },
  '7fd49ed7-a000-4f5a-8d97-b29cc1e587b9':
  {
    id: '7fd49ed7-a000-4f5a-8d97-b29cc1e587b9',
    displayName: 'Gergely Bereczki',
    jobTitle: 'Junior support engineer',
    mail: 'Gergely.Bereczki@memoQ.com',
    mobilePhone: '+36 30 573 5385',
    officeLocation: 'Gyula, Hungary',
    department: 'Support',
    city: null,
    country: null,
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  'ad08152e-1bcf-4706-8c88-631e5072592c':
  {
    id: 'ad08152e-1bcf-4706-8c88-631e5072592c',
    displayName: 'Gergely Garai',
    jobTitle: 'Technical Content Team Lead',
    mail: 'Gergely.Garai@memoQ.com',
    mobilePhone: '+36 20 415 7162',
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '17e6bcc1-121b-4693-a4a8-c82cbe6448d1'
  },
  'e546cab8-2f4e-48c6-b468-7228e92e60d8':
  {
    id: 'e546cab8-2f4e-48c6-b468-7228e92e60d8',
    displayName: 'Gergely Vándor',
    jobTitle: 'Product Manager',
    mail: 'Gergely.Vandor@memoQ.com',
    mobilePhone: '+36 30 659 01 64',
    officeLocation: 'Budapest, Hungary',
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '9b59c01a-8423-4d3f-abc1-2d115f7fc251'
  },
  '910e4fa3-f583-4e85-bbf4-72f37729460c':
  {
    id: '910e4fa3-f583-4e85-bbf4-72f37729460c',
    displayName: 'Gergő Vidó',
    jobTitle: 'Financial Administrator',
    mail: 'Gergo.Vido@memoQ.com',
    mobilePhone: '+36 70 396 5328',
    officeLocation: null,
    department: 'Finance',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '8900a9ef-08da-4c75-9edd-a47430ad397b'
  },
  '8a865732-6deb-4ad0-93cb-e1d9dea3be1d':
  {
    id: '8a865732-6deb-4ad0-93cb-e1d9dea3be1d',
    displayName: 'Gusztáv Jánvári',
    jobTitle: 'QA Manager and Product Designer',
    mail: 'Gusztav.Janvari@memoQ.com',
    mobilePhone: '+36 20 972 62 95',
    officeLocation: null,
    department: 'Production',
    city: null,
    country: null,
    mgrId: '17e6bcc1-121b-4693-a4a8-c82cbe6448d1'
  },
  'c872a1a7-fd38-4a50-80c9-d3ac141ce7cf':
  {
    id: 'c872a1a7-fd38-4a50-80c9-d3ac141ce7cf',
    displayName: 'János Szabó',
    jobTitle: 'Senior Support Engineer | Solution Engineer Associate',
    mail: 'Janos.Szabo@memoQ.com',
    mobilePhone: '+36 30 180 27 25',
    officeLocation: 'Gyula, Hungary',
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  'c6674814-3973-4fb4-a148-765635fcd26e':
  {
    id: 'c6674814-3973-4fb4-a148-765635fcd26e',
    displayName: 'Judit Árvay',
    jobTitle: 'SMA Account Manager',
    mail: 'Judit.Arvay@memoQ.com',
    mobilePhone: '+36 30 949 04 58',
    officeLocation: null,
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '70f69c66-794a-44b0-b52d-de1b7a628f9d':
  {
    id: '70f69c66-794a-44b0-b52d-de1b7a628f9d',
    displayName: 'Jure Dernovsek',
    jobTitle: 'Solution Engineer',
    mail: 'Jure.Dernovsek@memoQ.com',
    mobilePhone: '+38 64 080 1478',
    officeLocation: null,
    department: 'Smarketing',
    city: 'Ljubljana',
    country: 'Slovenia',
    mgrId: '9b59c01a-8423-4d3f-abc1-2d115f7fc251'
  },
  '4817fdc3-e24f-4d02-9fc9-fe1651a8a346':
  {
    id: '4817fdc3-e24f-4d02-9fc9-fe1651a8a346',
    displayName: 'Katalin Hollósi',
    jobTitle: 'Solution Architect',
    mail: 'Katalin.Hollosi@memoQ.com',
    mobilePhone: '+36 20 458 9749',
    officeLocation: null,
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '9b59c01a-8423-4d3f-abc1-2d115f7fc251'
  },
  '32977330-b540-4a4d-a55a-a79d6e634dcc':
  {
    id: '32977330-b540-4a4d-a55a-a79d6e634dcc',
    displayName: 'Kornél Páncél',
    jobTitle: 'Support Engineer',
    mail: 'Kornel.Pancel@memoQ.com',
    mobilePhone: '+36 70 563 7427',
    officeLocation: null,
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  'ef0ad431-d354-46a7-9083-cfeb3854b681':
  {
    id: 'ef0ad431-d354-46a7-9083-cfeb3854b681',
    displayName: 'Kristóf Katus',
    jobTitle: 'Software Developer',
    mail: 'Kristof.Katus@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '8ff7f11e-c7c6-4207-9523-46d32cd65d4f':
  {
    id: '8ff7f11e-c7c6-4207-9523-46d32cd65d4f',
    displayName: 'Krisztián Balázs',
    jobTitle: 'Support Engineer',
    mail: 'Krisztian.Balazs@memoQ.com',
    mobilePhone: '+36 30 239 1622',
    officeLocation: 'Gyula, Hungary',
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  '71a16b5b-df3e-43b2-a2a4-ac7b07feb647':
  {
    id: '71a16b5b-df3e-43b2-a2a4-ac7b07feb647',
    displayName: 'László Érsik',
    jobTitle: 'Junior Support Engineer',
    mail: 'Laszlo.Ersik@memoQ.com',
    mobilePhone: '+36 30 849 0427',
    officeLocation: 'Gyula, Hungary',
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  '55d33abd-c08d-45b0-b262-de41dbd307c6':
  {
    id: '55d33abd-c08d-45b0-b262-de41dbd307c6',
    displayName: 'Levente Galbáts',
    jobTitle: 'Solution Engineer',
    mail: 'Levente.Galbats@memoQ.com',
    mobilePhone: '+36 30 587 3968',
    officeLocation: 'Gyula, Hungary',
    department: 'Smarketing',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  'fbf9d4b3-cf28-41f6-96df-41b488a79e3e':
  {
    id: 'fbf9d4b3-cf28-41f6-96df-41b488a79e3e',
    displayName: 'Luka Juodelyte',
    jobTitle: 'Junior Business Development Representative',
    mail: 'Luka.Juodelyte@memoQ.com',
    mobilePhone: null,
    officeLocation: null,
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  'a0597452-9d45-40bc-bb23-be8604cfdc24':
  {
    id: 'a0597452-9d45-40bc-bb23-be8604cfdc24',
    displayName: 'Mária Baji-Weigert',
    jobTitle: 'Controller',
    mail: 'Maria.Baji-Weigert@memoQ.com',
    mobilePhone: null,
    officeLocation: null,
    department: 'Finance',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '8900a9ef-08da-4c75-9edd-a47430ad397b'
  },
  '308c1c63-6e44-4f4c-bec3-d798a2270f4c':
  {
    id: '308c1c63-6e44-4f4c-bec3-d798a2270f4c',
    displayName: 'Mária Petrusán',
    jobTitle: 'Administration Assistant',
    mail: 'Maria.Petrusan@memoQ.com',
    mobilePhone: '+36 70 701 30 85',
    officeLocation: 'Budapest, Hungary',
    department: 'Finance',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '8900a9ef-08da-4c75-9edd-a47430ad397b'
  },
  '2c58620c-4e66-4160-85ff-d02003dd5032':
  {
    id: '2c58620c-4e66-4160-85ff-d02003dd5032',
    displayName: 'Mariann Jávorszky',
    jobTitle: 'Junior developer',
    mail: 'Mariann.Javorszky@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '04a07d34-16f6-40dd-a88a-369d0182a9fc':
  {
    id: '04a07d34-16f6-40dd-a88a-369d0182a9fc',
    displayName: 'Márton Horváth',
    jobTitle: 'Head of Support',
    mail: 'Marton.Horvath@memoQ.com',
    mobilePhone: '+36 70 532 45 47',
    officeLocation: 'Gyula, Hungary',
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '411ddde4-c070-469a-8c1d-f615b0b9a85c'
  },
  '03e9cbf0-a158-4e7c-99c7-62bcbc367052':
  {
    id: '03e9cbf0-a158-4e7c-99c7-62bcbc367052',
    displayName: 'Maxim Dorkó',
    jobTitle: 'Solution Architect',
    mail: 'Maxim.Dorko@memoQ.com',
    mobilePhone: '+36 30 567 7898',
    officeLocation: null,
    department: 'Services & IT',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: 'b30295c2-10cd-4442-8dcf-02c661deb531'
  },
  '0453ecbf-c4ff-46ee-8d35-2488161b23ac':
  {
    id: '0453ecbf-c4ff-46ee-8d35-2488161b23ac',
    displayName: 'Mihály Hóbor',
    jobTitle: 'Junior Software Developer',
    mail: 'Mihaly.Hobor@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '6cbbce24-71c3-4459-b37a-96bb549ebd17':
  {
    id: '6cbbce24-71c3-4459-b37a-96bb549ebd17',
    displayName: 'Mike Wheeler',
    jobTitle: 'Sales Executive',
    mail: 'Mike.Wheeler@memoQ.com',
    mobilePhone: '+1 206 293 8694',
    officeLocation: null,
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '135afa2c-4382-41d7-a4da-d04e00e50d58':
  {
    id: '135afa2c-4382-41d7-a4da-d04e00e50d58',
    displayName: 'Nóra Giczy',
    jobTitle: 'Junior Developer',
    mail: 'Nora.Giczy@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '583094dd-4129-49c4-8e9f-0a8bd0a98199':
  {
    id: '583094dd-4129-49c4-8e9f-0a8bd0a98199',
    displayName: 'Norbert Dalecker',
    jobTitle: 'Software Developer',
    mail: 'Norbert.Dalecker@memoq.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '411ddde4-c070-469a-8c1d-f615b0b9a85c':
  {
    id: '411ddde4-c070-469a-8c1d-f615b0b9a85c',
    displayName: 'Norbert Oroszi',
    jobTitle: 'Chief Executive Officer',
    mail: 'Norbert.Oroszi@memoq.com',
    mobilePhone: '‭+41 78 402 55 24‬',
    officeLocation: 'Zürich, Switzerland',
    department: 'Management',
    city: 'Zürich',
    country: 'Switzerland'
  },
  '78b3d672-3a71-4c43-a51e-96ffbf668c5a':
  {
    id: '78b3d672-3a71-4c43-a51e-96ffbf668c5a',
    displayName: 'Norbert Szolnoki',
    jobTitle: 'Support Engineer',
    mail: 'Norbert.Szolnoki@memoq.com',
    mobilePhone: '+36 70 410 2217',
    officeLocation: null,
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  'f9aa4e85-ee4b-430e-a41d-34134809eab6':
  {
    id: 'f9aa4e85-ee4b-430e-a41d-34134809eab6',
    displayName: 'Patrick Molnár',
    jobTitle: 'Content Marketing Coordinator',
    mail: 'Patrick.Molnar@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '57a8d792-7f79-406f-b90c-8a146193f1e8':
  {
    id: '57a8d792-7f79-406f-b90c-8a146193f1e8',
    displayName: 'Péter Botta',
    jobTitle: 'DevOps Engineer',
    mail: 'Peter.Botta@memoq.com',
    mobilePhone: '+36 70 331 1568',
    officeLocation: 'Gyula, Hungary',
    department: 'Services & IT',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: 'b30295c2-10cd-4442-8dcf-02c661deb531'
  },
  '31b46b1f-31e8-4e43-b3e2-d65c98c54e10':
  {
    id: '31b46b1f-31e8-4e43-b3e2-d65c98c54e10',
    displayName: 'Péter Szögi',
    jobTitle: 'Support Engineer',
    mail: 'Peter.Szogi@memoq.com',
    mobilePhone: '+36 20 372 42 71',
    officeLocation: 'Gyula, Hungary',
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  '1cbe8c90-2095-4611-88b2-b1be5ffb6332':
  {
    id: '1cbe8c90-2095-4611-88b2-b1be5ffb6332',
    displayName: 'Richard Sikes',
    jobTitle: 'Solution Architect',
    mail: 'Richard.Sikes@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Toronto, Canada',
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '9b59c01a-8423-4d3f-abc1-2d115f7fc251'
  },
  '06ffc953-e3f5-4e52-9991-ad981fdc3f50':
  {
    id: '06ffc953-e3f5-4e52-9991-ad981fdc3f50',
    displayName: 'Rita Varga',
    jobTitle: 'Digital Marketing Manager',
    mail: 'Rita.Varga@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '9b46b858-e7c9-4718-981b-a0db6d6a6248':
  {
    id: '9b46b858-e7c9-4718-981b-a0db6d6a6248',
    displayName: 'Sándor Papp',
    jobTitle: 'Events Manager',
    mail: 'Sandor.Papp@memoq.com',
    mobilePhone: '+36 30 359 98 05',
    officeLocation: 'Budapest, Hungary',
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  'bfab5b13-2737-485f-8308-555eb91c2846':
  {
    id: 'bfab5b13-2737-485f-8308-555eb91c2846',
    displayName: 'Sandra Paulini',
    jobTitle: 'Key Account Manager',
    mail: 'Sandra.Paulini@memoQ.com',
    mobilePhone: null,
    officeLocation: null,
    department: 'Smarketing',
    city: 'Montreal',
    country: 'Canada',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '369743bd-f6b8-4695-a87b-a92921f96f8b':
  {
    id: '369743bd-f6b8-4695-a87b-a92921f96f8b',
    displayName: 'Szabolcs Buslig',
    jobTitle: 'Software Architect',
    mail: 'Szabolcs.Buslig@memoq.com',
    mobilePhone: '+40 746 06 8833',
    officeLocation: null,
    department: 'Services & IT',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: 'b30295c2-10cd-4442-8dcf-02c661deb531'
  },
  'fb5c5e1b-4e32-44ae-833c-b9db2ce39443':
  {
    id: 'fb5c5e1b-4e32-44ae-833c-b9db2ce39443',
    displayName: 'Tamás Rell',
    jobTitle: 'UX Designer',
    mail: 'Tamas.Rell@memoq.com',
    mobilePhone: '+36-70-541-3385',
    officeLocation: null,
    department: 'Production',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '24308347-aff3-4378-9539-13cf932e4139':
  {
    id: '24308347-aff3-4378-9539-13cf932e4139',
    displayName: 'Tamás Ritter',
    jobTitle: 'Academic And Individual Translator Sales',
    mail: 'Tamas.Ritter@memoq.com',
    mobilePhone: '+36 70 669 89 90',
    officeLocation: 'Gyula, Hungary',
    department: 'Smarketing',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b':
  {
    id: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b',
    displayName: 'Tamás Tőkés',
    jobTitle: 'Senior Software Developer',
    mail: 'Tamas.Tokes@memoq.com',
    mobilePhone: '+36 (70) 220-1372',
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '17e6bcc1-121b-4693-a4a8-c82cbe6448d1'
  },
  '7cba6966-5efd-402d-b408-01a6e680d932':
  {
    id: '7cba6966-5efd-402d-b408-01a6e680d932',
    displayName: 'Ulrich Fricke',
    jobTitle: 'Account Manager',
    mail: 'Ulrich.Fricke@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Contractor',
    department: 'Smarketing',
    city: null,
    country: null,
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '672bf9fd-d2fd-4e29-980a-90d0f9f73052':
  {
    id: '672bf9fd-d2fd-4e29-980a-90d0f9f73052',
    displayName: 'Veronika Pándi',
    jobTitle: 'Software Designer',
    mail: 'Veronika.Pandi@memoq.com',
    mobilePhone: '+36 30 296 77 08',
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '17e6bcc1-121b-4693-a4a8-c82cbe6448d1'
  },
  'dae21b54-0548-41e4-897a-6c0d76d152b1':
  {
    id: 'dae21b54-0548-41e4-897a-6c0d76d152b1',
    displayName: 'Viktor Lengyel',
    jobTitle: 'Support Engineer',
    mail: 'Viktor.Lengyel@memoq.com',
    mobilePhone: '+36 30 856 3838',
    officeLocation: null,
    department: 'Support',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '04a07d34-16f6-40dd-a88a-369d0182a9fc'
  },
  '9ed507b2-0f30-4d15-97d2-ceaef3f936f9':
  {
    id: '9ed507b2-0f30-4d15-97d2-ceaef3f936f9',
    displayName: 'Viktor Mochnács',
    jobTitle: 'Technical Content Specialist',
    mail: 'Viktor.Mochnacs@memoq.com',
    mobilePhone: '+36 (30) 299-4770',
    officeLocation: null,
    department: 'Production',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: 'ad08152e-1bcf-4706-8c88-631e5072592c'
  },
  '9b893874-00ec-4da4-9ec3-53c3cc5ecfa5':
  {
    id: '9b893874-00ec-4da4-9ec3-53c3cc5ecfa5',
    displayName: 'Yo Miura',
    jobTitle: 'Sales representative for Japan',
    mail: 'Yo.Miura@memoq.com',
    mobilePhone: '+36 30 349 37 58',
    officeLocation: 'Budapest, Hungary',
    department: 'Smarketing',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '33b3f0ee-52cc-409f-a294-ae76f2397b96'
  },
  '8900a9ef-08da-4c75-9edd-a47430ad397b':
  {
    id: '8900a9ef-08da-4c75-9edd-a47430ad397b',
    displayName: 'Zoltán Honti',
    jobTitle: 'Head of Finance',
    mail: 'Zoltan.Honti@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Finance',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '411ddde4-c070-469a-8c1d-f615b0b9a85c'
  },
  'b30295c2-10cd-4442-8dcf-02c661deb531':
  {
    id: 'b30295c2-10cd-4442-8dcf-02c661deb531',
    displayName: 'Zsolt Farkas',
    jobTitle: 'Head of Services and Infrastructure',
    mail: 'Zsolt.Farkas@memoq.com',
    mobilePhone: '+36 30 205 9722',
    officeLocation: 'Gyula, Hungary',
    department: 'Services & IT',
    city: 'Gyula',
    country: 'Hungary',
    mgrId: '411ddde4-c070-469a-8c1d-f615b0b9a85c'
  },
  '4ebb6dbb-9a08-4105-a19c-34bcb6bbd68b':
  {
    id: '4ebb6dbb-9a08-4105-a19c-34bcb6bbd68b',
    displayName: 'Zsolt Fülöp',
    jobTitle: 'Senior Software Developer',
    mail: 'Zsolt.Fulop@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  'b4900186-149d-4081-bf6b-c8252efbedcb':
  {
    id: 'b4900186-149d-4081-bf6b-c8252efbedcb',
    displayName: 'Zsolt Molnár',
    jobTitle: 'Software Developer',
    mail: 'Zsolt.Molnar@memoQ.com',
    mobilePhone: null,
    officeLocation: 'Budapest, Hungary',
    department: 'Production',
    city: null,
    country: null,
    mgrId: '4d7b85b7-cb5c-4e9a-a42f-e58a1092760b'
  },
  '54f3c712-c777-4992-97bd-87b56cced33e':
  {
    id: '54f3c712-c777-4992-97bd-87b56cced33e',
    displayName: 'Zsolt Varga',
    jobTitle: 'Software Designer',
    mail: 'Zsolt.Varga@memoq.com',
    mobilePhone: '+36 30 518 9817',
    officeLocation: null,
    department: 'Production',
    city: 'Budapest',
    country: 'Hungary',
    mgrId: '17e6bcc1-121b-4693-a4a8-c82cbe6448d1'
  }
};

module.exports = {
  getUserDetails: async function (accessToken) {
    const client = getAuthenticatedClient(accessToken);

    const user = await client.api('/me').get();
    return user;
  },

  getOrg: async function (accessToken) {
    const client = getAuthenticatedClient(accessToken);
    const users = await client
      .api('/users?$top=999')
      .select('id,displayName,jobTitle,mail,mobilePhone,officeLocation,department,city,country')
      .get();
    var idToUser = {};
    for (var i = 0; i < users.value.length; ++i) {
      var x = users.value[i];
      idToUser[x.id] = x;
    }
    var toDel = [];
    var ceo = null;
    for (var id in idToUser) {
      if (!idToUser.hasOwnProperty(id)) continue;
      if (idToUser[id].jobTitle == "Chief Executive Officer") {
        ceo = idToUser[id];
        continue;
      }
      try {
        const mgrId = await client
          .api('/users/' + id + '/manager')
          .select('id')
          .get();
        idToUser[id].mgrId = mgrId.id;
      }
      catch {
        toDel.push(id);
      }
    }
    for (var i = 0; i < toDel.length; ++i) {
      var id = toDel[i];
      delete idToUser[id];
    }

    // !!
    // var idToUser = JSON.parse(JSON.stringify(xdata));
    // var ceo = idToUser['411ddde4-c070-469a-8c1d-f615b0b9a85c'];

    for (var id in idToUser) {
      if (!idToUser.hasOwnProperty(id)) continue;
      var usr = idToUser[id];
      // CEO has no manager, so skip
      if (usr === ceo) continue;
      var mgr = idToUser[usr.mgrId];
      if (!mgr.hasOwnProperty("reports")) mgr.reports = [];
      mgr.reports.push(usr);
    }
    return ceo;
  }
};

function getAuthenticatedClient(accessToken) {
  // Initialize Graph client
  const client = graph.Client.init({
    // Use the provided access token to authenticate
    // requests
    authProvider: (done) => {
      done(null, accessToken);
    }
  });

  return client;
}