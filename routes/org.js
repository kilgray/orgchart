var express = require('express');
var router = express.Router();
var tokens = require('../tokens.js');
var graph = require('../graph.js');

var tree =
{
  "displayName": "Clifford Shanks",
  "jobTitle": 1862,
  "department": 1906,
  "location": "Petersburg, VA",
  "reports": [
    {
      "displayName": "James Shanks",
      "jobTitle": 1831,
      "department": 1884,
      "location": "Petersburg, VA",
      "reports": [
        {
          "displayName": "Robert Shanks",
          "jobTitle": 1781,
          "department": 1871,
          "location": "Ireland/Petersburg, VA"
        },
        {
          "displayName": "Elizabeth Shanks",
          "jobTitle": 1795,
          "department": 1871,
          "location": "Ireland/Petersburg, VA",
          "reports": [
            {
              "displayName": "Henry Brown",
              "jobTitle": 1792,
              "department": 1845,
              "location": "Montgomery, NC"
            },
            {
              "displayName": "Sarah Houchins",
              "jobTitle": 1793,
              "department": 1882,
              "location": "Montgomery, NC"
            }
          ]
        }
      ]
    },
    {
      "displayName": "Ann Emily Brown",
      "jobTitle": 1826,
      "department": 1866,
      "location": "Brunswick/Petersburg, VA",
      "reports": [
        {
          "displayName": "Henry Brown",
          "jobTitle": 1792,
          "department": 1845,
          "location": "Montgomery, NC"
        },
        {
          "displayName": "Sarah Houchins",
          "jobTitle": 1793,
          "department": 1882,
          "location": "Montgomery, NC"
        },
        {
          "displayName": "Henry Brown",
          "jobTitle": 1792,
          "department": 1845,
          "location": "Montgomery, NC"
        },
        {
          "displayName": "Sarah Houchins",
          "jobTitle": 1793,
          "department": 1882,
          "location": "Montgomery, NC"
        }
      ]
    },
    {
      "displayName": "James Shanks",
      "jobTitle": 1831,
      "department": 1884,
      "location": "Petersburg, VA",
      "reports": [
        {
          "displayName": "Robert Shanks",
          "jobTitle": 1781,
          "department": 1871,
          "location": "Ireland/Petersburg, VA"
        },
        {
          "displayName": "Elizabeth Shanks",
          "jobTitle": 1795,
          "department": 1871,
          "location": "Ireland/Petersburg, VA"
        }
      ]
    },
    {
      "displayName": "Ann Emily Brown",
      "jobTitle": 1826,
      "department": 1866,
      "location": "Brunswick/Petersburg, VA",
      "reports": [
        {
          "displayName": "Henry Brown",
          "jobTitle": 1792,
          "department": 1845,
          "location": "Montgomery, NC"
        },
        {
          "displayName": "Sarah Houchins",
          "jobTitle": 1793,
          "department": 1882,
          "location": "Montgomery, NC"
        }
      ]
    }
  ]
};

router.get('/',
  async function (req, res) {
    // DBG
    // var org = await graph.getOrg(accessToken);
    // return res.json(org);

    // Get the access token
    var accessToken = null;
    try {
      accessToken = await tokens.getAccessToken(req);
    }
    catch (err) {
      return res.status(400).send("Error getting access token.");
    }
    if (!accessToken || accessToken.length == 0) {
      return res.status(400).send("No access token; login needed.");
    }
    // Retrieve organization from MS Graph
    try {
      var org = await graph.getOrg(accessToken);
      return res.json(org);
    }
    catch (err) {
      return res.status(400).send("Could not retrieve organization.");
    }
  }
);

module.exports = router;